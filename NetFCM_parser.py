#!/usr/bin/python
import re
import csv
import getopt
import os
from sys import exc_info, argv, exit
from optparse import OptionParser
import subprocess
from distutils import dir_util
import shutil
import zipfile
import glob


def make_gating_list(parsed_input_dictionary):
	gating_marker_keys = []
	for i in parsed_input_dictionary.keys():
		if re.search("^-gm",i):
			gating_marker_keys.append(i)
	gating_list = [0]*len(gating_marker_keys)
	for s in gating_marker_keys:
		digit = re.findall('\d+', s) 
		gate_key = '-g' + digit[0]
		parameter_key = '-p' + digit[0]
		evd_option_key = '-c' + digit[0]
		if (parsed_input_dictionary[evd_option_key] == ""):
			parsed_input_dictionary[evd_option_key] = 0                    
		gating_list[int(digit[0])-1] = [parsed_input_dictionary[s],parsed_input_dictionary[gate_key], parsed_input_dictionary[parameter_key], parsed_input_dictionary[evd_option_key]]
	new_gating_list = []
	for i in range(len(gating_list)):
		#if (gating_list[i][1] == "evd"):
		#	evd_check = "TRUE"
		if not gating_list[i][0] == "" and not gating_list[i][1] == "":
			new_gating_list.append(gating_list[i])
	return(new_gating_list)

def zipdir(path, zip):
    for root, dirs, files in os.walk(path):
        for file in files:
            zip.write(os.path.join(root, file))
	
def make_translation_file_list(parsed_input_dictionary):
	fl_keys = []
	for i in parsed_input_dictionary.keys():
		if re.search("^-fl",i):
			fl_keys.append(i)
	translation_list = [0]*len(fl_keys)
	for s in fl_keys:
		digit = re.findall('\d+', s) 
		marker_key = '-m' + digit[0]
		# this #prints out the translation table as a list:
		translation_list[int(digit[0])-1] = [parsed_input_dictionary[marker_key],parsed_input_dictionary[s]]
	# get rid of empty lists
	new_translation_list = []
	for i in range(len(translation_list)):
		if not translation_list[i][0] == "" and not translation_list[i][1] == "":
			new_translation_list.append(translation_list[i])
	return(new_translation_list)

def parse(splitstring):
	#splitstring = re.split("\s",string)
	counts = 0
	parsed_input_dictionary = {}
	key = ''
	for element in splitstring:
		if re.search("^-+[A-Za-z]",element):
			key = element
			parsed_input_dictionary[key]= []
		elif key == '':
			 pass
		else:
			parsed_input_dictionary[key].append(element)
			
	return(parsed_input_dictionary)

def editparse(parsed_input_dictionary):
	for key in parsed_input_dictionary.keys():
		values = parsed_input_dictionary[key]
		parsed_input_dictionary[key] = ' '.join(values)
	return(parsed_input_dictionary)

def main(argv):
	
	
	# to revert: uncomment the following line:
	splitstring = argv
	evd_check = "FALSE"
	parsed_input_dictionary = parse(splitstring)
	parsed_input_dictionary = editparse(parsed_input_dictionary)
	current_run = parsed_input_dictionary["--runroot"]
	translationstate = parsed_input_dictionary["-trs"]
	current_folder = '/home/data21/services/NetFCM/NetFCM-1.1/IO/' +current_run + '/'
	current_folder_cgebase2 = '/home/data1/services/NetFCM/NetFCM-1.1/IO/' +current_run + '/'
	fcs_dir = current_folder + 'uploaded'
	fcs_dir_cgebase2 = current_folder_cgebase2 + 'uploaded'
	csv_dir = current_folder + 'intermediate'
	csv_dir_cgebase2 = current_folder_cgebase2 + 'intermediate'
	results_fig_dir = current_folder + 'results/figures'
	results_fig_dir_cgebase2 = current_folder_cgebase2 + 'results/figures'
	results_csv_dir = current_folder + 'results/csv'
	results_csv_dir_cgebase2 = current_folder_cgebase2 + 'results/csv'
	results_dir = current_folder + 'results'
	results_zscore_dir = current_folder + 'results/zscore'
	results_zscore_dir_cgebase2 = current_folder + 'results/zscore'
	NetFCM_r = '/home/data1/services/NetFCM/NetFCM-1.1/scripts/NetFCM_servers.R'
	NetFCM_r_test = '/home/data1/services/NetFCM/NetFCM-1.1/scripts/Check.R'
	results_heatmap_dir = current_folder + 'results/heatmaps'
	results_heatmap_dir_cgebase2 = current_folder_cgebase2 + 'results/heatmaps'
	
	
	if not os.path.isdir(fcs_dir):
		print "Error: There were no uploaded files."
		exit()
	
		
	if not os.path.exists(results_dir):
		os.umask(0)
		os.makedirs(results_dir)
		os.chmod(results_dir,0777)
		
	if not os.path.exists(results_heatmap_dir):
		os.umask(0)
		os.makedirs(results_heatmap_dir)
		os.chmod(results_heatmap_dir,0777)

	if not os.path.exists(csv_dir):
		os.umask(0)
		os.makedirs(csv_dir)
		os.chmod(csv_dir,0777)
	
	if not os.path.exists(results_fig_dir):
		os.umask(0)
		os.makedirs(results_fig_dir)
		os.chmod(results_fig_dir,0777)
	
	if not os.path.exists(results_csv_dir):
		os.umask(0)
		os.makedirs(results_csv_dir)
		os.chmod(results_csv_dir,0777)
	
	translation_list = make_translation_file_list(parsed_input_dictionary)
	
	if not translation_list:
		print "The Translation table was not filled out, please see click on the \"Instruction\" tab for details on how to do so."
		exit()
	
	# for server:
	csv_trans = csv_dir +'/translationfile.csv'
	trans_out = open(csv_trans,"wb")
	mywriter = csv.writer(trans_out)
	for row in translation_list:
	    mywriter.writerow(row)
	trans_out.close()
	
	gating_list = make_gating_list(parsed_input_dictionary)
	
	csv_trans_cgebase2 = csv_dir_cgebase2 + '/translationfile.csv'
	
	
	if not gating_list:
		print "The Gating Strategy was not filled out, please see click on the \"Instruction\" tab for details on how to do so."
		exit()
	
	# for server:
	csv_gating = csv_dir +'/gatingfile.csv'
	gating_out = open(csv_gating,"wb")
	mywriter = csv.writer(gating_out)
	for row in gating_list:
		mywriter.writerow(row)
	gating_out.close()
	
	csv_gating_cgebase2 = csv_dir_cgebase2 + '/gatingfile.csv'
	
	try:
		command = "ssh cgebase2 \"/usr/lib64/R/bin/Rscript "+ NetFCM_r_test +" "+ fcs_dir_cgebase2 +" "+ csv_gating_cgebase2 +" "+ csv_trans_cgebase2 + " " +current_folder_cgebase2+" >& "+ csv_dir_cgebase2 +"/err_test\""
		proc = subprocess.Popen(command, shell=True)
		proc.wait()
		success = 1
	
	except:
		exit("Something went wrong, please try again..")
	
	if translationstate == '':
		translationstate = 0
	
	
	type_one_error_file_path = current_folder + 'Error_type1.txt'
	type_two_error_file_path = current_folder + 'Error_type2.txt'
	type_three_error_file_path = current_folder + 'Error_type3.txt'
	type_four_error_file_path = current_folder + 'Error_type4.txt'
	type_five_error_file_path = current_folder + 'Error_type5.txt'
	
	if os.path.exists(type_one_error_file_path):
		print "There were inconsistencies in the headers of the uploaded FCS files."
		exit()
	
	if os.path.exists(type_two_error_file_path):
		print "The FCS names given in the translation table did not match any of the names in the header of the uploaded FCS files. Please try again."
		exit()
	
	if os.path.exists(type_three_error_file_path):
		print "The marker names given in the gating strategy do not match up to any of the names in the translation table nor to any of the names in the header of the uploaded FCS files. Please try again."
		exit()
		
	if os.path.exists(type_four_error_file_path):
		print "NetFCM-1.1 only performs K-means clustering on one marker at a time. In your gating strategy you entered more than one marker for the gate type \"K-means\""
		exit()
	
	if os.path.exists(type_five_error_file_path):
		print "NetFCM-1.1 only allows for two boundary conditions. Please try again with two boundary conditions for the Parameter values box for the gate type \"PCA\""
		exit()
	
	command = "echo $LD_LIBRARY_PATH > "+ csv_dir +"/err_test1"
	proc = subprocess.Popen(command, shell=True)
	proc.wait()
	try:
		
		command2 = "ssh cgebase2 \"/usr/lib64/R/bin/Rscript "+ NetFCM_r +" "+ fcs_dir_cgebase2 +" "+ csv_gating_cgebase2 +" "+ csv_trans_cgebase2 +" "+ results_fig_dir_cgebase2 +" "+ results_csv_dir_cgebase2+" " +results_heatmap_dir_cgebase2+" "+csv_dir_cgebase2+ " " + str(translationstate) +" >& "+ csv_dir_cgebase2 +"/err\""
		proc2 = subprocess.Popen(command2, shell=True)
		proc2.wait()
		
		
		results_cge = '/srv/www/htdocs/services/NetFCM-1.1/tmp/' + current_run

		if not os.path.exists(results_cge):
			os.umask(0)
			os.makedirs(results_cge)
			os.chmod(results_cge,0777)
		
		results_cge_fig_dir = results_cge + "/figures"
		
		if not os.path.exists(results_cge_fig_dir):
			os.umask(0)
			os.makedirs(results_cge_fig_dir)
			os.chmod(results_cge_fig_dir,0777)
			
		results_cge_heatmap_dir = results_cge + "/heatmaps"
		
		if not os.path.exists(results_cge_heatmap_dir):
			os.umask(0)
			os.makedirs(results_cge_heatmap_dir)
			os.chmod(results_cge_heatmap_dir,0777)
		
		
		# This transfers the figures to the tmp folder on CGE where it is available to be published.
		
		#results_fig_dir_cge = re.sub("data1","data21",results_fig_dir)
		fig_files = os.listdir(results_fig_dir)

		for file_name in fig_files:
			png_name = re.sub(".pdf",".png",file_name)
			full_file_name = os.path.join(results_fig_dir,file_name)
			full_png_name = os.path.join(results_cge_fig_dir,png_name)
			if (os.path.isfile(full_file_name)):
				subprocess.call(["convert",full_file_name,full_png_name])		
		
		#results_heatmap_dir_cge = re.sub("data1","data21",results_heatmap_dir)
		heatmap_files = os.listdir(results_heatmap_dir)
		if len(heatmap_files) > 0:
			for file_name in heatmap_files:
				png_name = re.sub(".pdf",".png",file_name)
				full_file_name = os.path.join(results_heatmap_dir,file_name)
				full_png_name = os.path.join(results_cge_heatmap_dir,png_name)
				if (os.path.isfile(full_file_name)):
					subprocess.call(["convert",full_file_name,full_png_name])
					
		
		
		
		
		# heatmap for the Z-score pair-wise comparison of the samples are shown here:

		if (os.path.isfile(os.path.join(results_cge_heatmap_dir,"comparison.png"))):
			zscore_cge = results_cge + "/zscore"
			
			if not os.path.exists(zscore_cge):
				os.umask(0)
				os.makedirs(zscore_cge)
				os.chmod(zscore_cge,0777)
				
			if (os.path.isfile(os.path.join(results_cge_heatmap_dir,"comparison_long.png"))):
				print "<br>"
				print "<h2>Pattern of Responses</h2>"
				print "<img src=\"http://cge.cbs.dtu.dk/services/NetFCM-1.1/tmp/" + current_run + "/heatmaps/comparison_long.png \" alt=''>"
				full_file_name_long = os.path.join(csv_dir,"comparison_zscoreLstMat.xls")
				if (os.path.isfile(full_file_name_long)):
					subprocess.call(["cp",full_file_name_long,zscore_cge])
					print "<br><br>"
					print "To download the xls file for the heat map, please click the link below:<br>"
					print "<a href=\"http://cge.cbs.dtu.dk/services/NetFCM-1.1/tmp/" + current_run + "/zscore/comparison_zscoreLstMat.xls" + "\"> "+"comparison_long.xls"+" </a>"
					print "<br>"
		
		if (os.path.isfile(os.path.join(results_cge_heatmap_dir,"comparison.png"))):
			print "<h1>Sample Comparison Results</h1>"
			print "<br>"
			print "<h2>Magnitude of Response</h2>"
			print "<img src=\"http://cge.cbs.dtu.dk/services/NetFCM-1.1/tmp/" + current_run + "/heatmaps/comparison.png \" alt=''>"
			#evd_check = "TRUE"
			zscore_cge = results_cge + "/zscore"
			
			if not os.path.exists(zscore_cge):
				os.umask(0)
				os.makedirs(zscore_cge)
				os.chmod(zscore_cge,0777)
			full_file_name = os.path.join(csv_dir,"comparison.xls")
			if (os.path.isfile(full_file_name)):
				subprocess.call(["cp",full_file_name,zscore_cge])
				
				print "<br><br>To download the xls file for the heat map, please click the link below:<br>"
				print "<a href=\"http://cge.cbs.dtu.dk/services/NetFCM-1.1/tmp/" + current_run + "/zscore/comparison.xls" + "\"> "+"comparison.xls"+" </a>"
				print "<br>"
			
			
			full_file_name_pval = os.path.join(csv_dir,"pvalMat.xls")
			
			if (os.path.isfile(full_file_name_pval)):
				subprocess.call(["cp",full_file_name_pval,zscore_cge])
				print "<br>"
				print "To download the xls file for the p values, please click the link below:<br>"
				print "<a href=\"http://cge.cbs.dtu.dk/services/NetFCM-1.1/tmp/" + current_run + "/zscore/pvalMat.xls" + "\"> "+"pvalues.xls"+" </a>"
				print "<br>"
			
			
			
			
			
		
		#else:
		#	print "There was an error."
		
		# for the figures:
		print "<h1>Gating Results</h1>"
		
		# Here's the list of figures
		cge_fig_files = os.listdir(results_cge_fig_dir)
		
		# Get the unique list of samples:
		new_cge_fig_files = [x.split("_figs_") for x in cge_fig_files]
		new_new_cge_fig_files = list(set(zip(*new_cge_fig_files)[0]))
		
		
		for sample in new_new_cge_fig_files:
			filtered_list_current = filter(lambda x:sample in x,cge_fig_files)
			print "<h2>"+"File:"+sample+"</h2>" 
			for row in gating_list:
				if row[1]=="pca":
					fig_text = "PCA: " + row[2]
					i = row[0]
					i = re.sub(" ", "_", i)
					i = re.sub("\.", "_", i)
					i = re.sub("/", "_", i)
					i = re.sub(",", "_", i)
					figure_files = filter(lambda x:i in x,filtered_list_current)
					figure_file1 = filter(lambda x:"before" in x,figure_files)[0]
					figure_file2 = filter(lambda x:"after" in x,figure_files)[0]
					print "<img src=\"http://cge.cbs.dtu.dk/services/NetFCM-1.1/tmp/" + current_run + "/figures/"+ figure_file1 +"\" alt='"+fig_text+"'>"
					print "<img src=\"http://cge.cbs.dtu.dk/services/NetFCM-1.1/tmp/" + current_run + "/figures/"+ figure_file2 +"\" alt='"+fig_text+"'>"
					print "<br><br>"
					
				elif row[1]=="clustering":
					fig_text = "Clustering: " + row[2]
					i = row[0]
					i = re.sub(" ", "_", i)
					i = re.sub("\.", "_", i)
					i = re.sub("/", "_", i)
					i = re.sub(",", "_", i)
					figure_file = filter(lambda x:i in x,filtered_list_current)[0]
					print "<img src=\"http://cge.cbs.dtu.dk/services/NetFCM-1.1/tmp/" + current_run + "/figures/"+ figure_file +"\" alt='"+fig_text+"'>"	
					print "<br><br>"

		# for the csv:
		csv_files = os.listdir(results_csv_dir)
		csv_cge = results_cge + "/csv"
		
		if not os.path.exists(csv_cge):
			os.umask(0)
			os.makedirs(csv_cge)
			os.chmod(csv_cge,0777)
		
		print "To download the result files for each sample, please click the link(s) below:<br>"
		
		for file_name in csv_files:
			full_file_name = os.path.join(results_csv_dir,file_name)
			if (os.path.isfile(full_file_name)):
				subprocess.call(["cp",full_file_name,csv_cge])
				print "<a href=\"http://cge.cbs.dtu.dk/services/NetFCM-1.1/tmp/" + current_run + "/csv/"+ file_name + "\"> "+file_name+" </a>"
				print "<br>"
		
		
	except:
		print "There was an error encountered..."
		a,b=exc_info()[:2]
		print 'Error name: %s' % a.__name__
		print 'Error code: %s' % b.args[0]
		
	

if __name__ == '__main__':
    main(argv[1:])
